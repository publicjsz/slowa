#! /bin/python3

import tqdm
import gzip
import csv
import pickle

from word_with_attrs import CWordWithAttrs


allowedLens = {5, 6}


if __name__ == '__main__':
    # reading the all words into memowy

    arrSlowa = []
    setBaseWords = set()
    setGameWords = set()

    counter1 = tqdm.tqdm(desc='read game words', unit_scale=True)
    with gzip.open('slowa.txt.gz', 'rt', encoding='utf-8') as file:
        while True:
            line = file.readline().strip().lower()
            if line == '':
                break

            setGameWords.add(line)
            counter1.update()
    counter1.close()

    counter2 = tqdm.tqdm(desc='read words', unit_scale=True)
    with gzip.open('odm.txt.gz', 'rt', encoding='utf-8') as file:
        while True:
            line = file.readline().strip().lower()
            if line == '':
                break

            words = line.split(', ')

            if words[0] in setGameWords:
                if len(words[0]) in allowedLens:
                    setBaseWords.add(words[0])

                for word in words:
                    if len(word) in allowedLens:
                        arrSlowa.append(word)

                counter2.update()
    counter2.close()

    # minLen = len(arrSlowa[0])
    # maxLen = len(arrSlowa[0])

    # for slowo in tqdm.tqdm(arrSlowa, desc='len counting', unit_scale=True):
    #     minLen = len(slowo) if len(slowo) < minLen else minLen
    #     maxLen = len(slowo) if len(slowo) > maxLen else maxLen

    # print(f"minLen = {minLen}, maxLen = {maxLen}")

    # creating len sets
    lenSets = {}
    for word in tqdm.tqdm(arrSlowa, desc='lenSets', unit_scale=True):
        slowoLen = len(word)

        if slowoLen in allowedLens:
            lenKey = f"len{len(word)}"

            if not lenKey in lenSets:
                lenSets[lenKey] = set()

            lenSets[lenKey].add(word)

    # creating letter sets
    letterSets = {}
    for word in tqdm.tqdm(arrSlowa, desc='letterSets', unit_scale=True):
        slowoLen = len(word)

        if slowoLen in allowedLens:
            for letter in word:
                if not letter in letterSets:
                    letterSets[letter] = set()

                letterSets[letter].add(word)

    # with open('base_words.csv', 'w', encoding='UTF8', newline='') as f:
    #     writer = csv.writer(f)

    #     for slowo in setBaseWords:
    #         l = len(slowo)

    #         if (l == 5) or (l == 6):
    #             writer.writerow([slowo])

    # exit(1)

    dictWordsWithAttrs = {}

    # read words from human file

    with open('human_words_points.csv', 'r', encoding='UTF8', newline='') as f:
        reader = csv.DictReader(f)

        for row in reader:
            word = row["słowo"]

            if word in dictWordsWithAttrs:
                cword = dictWordsWithAttrs[word]
            else:
                cword = CWordWithAttrs(word)
                dictWordsWithAttrs[word] = cword

            cword.setHumanPoints(float(row['punkty']))

    for lenSet in lenSets:
        setSlowa = lenSets[lenSet]

        for word in setSlowa:
            if not (word in dictWordsWithAttrs):
                cword = CWordWithAttrs(word)
                dictWordsWithAttrs[word] = cword

    for word in tqdm.tqdm(dictWordsWithAttrs, desc='letter points', unit_scale=True):
        cword = dictWordsWithAttrs[word]
        setSlowaWithLetters = set()
        for l in cword.word:
            if l in letterSets:
                setSlowaWithLetters |= letterSets[l]

        cword.setLetterPoints(len(setSlowaWithLetters))
        cword.setIsBaseWord(word in setBaseWords)

    with open("5_6_words.pickle", "bw") as file:
        pickle.dump({
            "setBaseWords": setBaseWords,
            "lenSets": lenSets,
            "letterSets": letterSets,
            "dictWordsWithAttrs": dictWordsWithAttrs
        },
            file)
