#! /bin/python3

import math


class CWordWithAttrs:

    def __init__(self, word: str):
        self.word = word
        self.len_points = len(self.word)
        self.letter_points = None
        self.human_points = None
        self.is_base_word = None

    def setLetterPoints(self, letter_points):
        self.letter_points = letter_points

    def setHumanPoints(self, human_points: float):
        self.human_points = human_points

    def setIsBaseWord(self, is_base_word):
        self.is_base_word = is_base_word

    @classmethod
    def csv_header(cls):
        return ['word', 'len', 'lp', 'hp']

    def csv_row(self):
        return [self.word, self.len_points, self.letter_points, self.human_points]

    
    def lt_word_popularity(self, other):
        if self.is_base_word and not other.is_base_word:
            return True

        if other.is_base_word and not self.is_base_word:
            return False

        hp = self.human_points or 0
        ln_hp = int(math.log(hp))+1 if hp > 0 else 0
        other_hp = other.human_points or 0
        other_ln_hp = int(math.log(other_hp))+1 if other_hp > 0 else 0
        
        if (ln_hp) != (other_ln_hp):
            return (ln_hp) > (other_ln_hp)

        if (self.letter_points or 0) != (other.letter_points or 0):
            return (self.letter_points or 0) > (other.letter_points or 0)

        if (self.len_points or 0) != (other.len_points or 0):
            return (self.len_points or 0) < (other.len_points or 0)

        return (self.word or '') < (other.word or '')

    def lt_letters_popularity(self, other):
        if (self.letter_points or 0) != (other.letter_points or 0):
            return (self.letter_points or 0) > (other.letter_points or 0)

        return (self.word or '') < (other.word or '')

    
    
    
    
    
    def __lt__(self, other):
        return self.lt_word_popularity(other)


    def cword_info(self):
        return f"(bw: {self.is_base_word}, hp: {self.human_points}, lp: {self.letter_points})"



