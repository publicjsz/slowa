#! /bin/python3

import re


def input_colorama(message, pattern=None, errorMessage=None, extraWords=None, onlyAllowedWords=None):
    while True:
        print(message, end='')
        word = input()
        if ((onlyAllowedWords is not None) and (word not in onlyAllowedWords)) or ((pattern is not None) and (not re.match(pattern, word) and (extraWords is None or word not in extraWords))):
            if errorMessage is not None:
                print(errorMessage)
            continue
        return word
