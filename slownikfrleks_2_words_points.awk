BEGIN {
	FS = "="
	OFS = ";"
}


(NF == 2) && ((length($1) == 5) || (length($1) == 6)) {
	print $1, $2
}