#! /bin/python3

import pickle
import tools
from word_with_attrs import CWordWithAttrs

from colorama import Fore
from colorama import Style


class CSlowaSolver:

    def __init__(self, tylko_podstawowe_slowa: bool):
        self.tylko_podstawowe_slowa = tylko_podstawowe_slowa

        print("Ładuję dane...")

        with open("5_6_words.pickle", "br") as file:
            obj = pickle.load(file)
            self.setBaseWords = obj['setBaseWords']
            self.lenSets = obj['lenSets']
            self.letterSets = obj['letterSets']
            self.dictWordsWithAttrs = obj['dictWordsWithAttrs']

    @classmethod
    def serve_special_words(cls, word: str, arrPasujaceSlowaAttr):
        if word == '-list':
            i = 0
            for cslowo in arrPasujaceSlowaAttr:
                format = f"{Style.RESET_ALL}"
                if (cslowo.human_points or 0) > 0:
                    format += f"{Style.BRIGHT}"

                if cslowo.is_base_word:
                    format += f"{Fore.GREEN}"
                else:
                    format += f"{Fore.YELLOW}"

                print(
                    f"{format}{cslowo.word}{Style.RESET_ALL} {cslowo.cword_info()}")
                i += 1
                if i > 10:
                    break

            return True

        return False

    @classmethod
    def calc_status_for_word(cls, word_to_guess: str, word: str):
        """
            return string 'opb' where:
            o - the letter is 'ok' (on proper position)
            p - the letter is on wrong position
            b - the letter does not exists in word
        """

        assert (len(word_to_guess) == len(word))

        ret = ''

        for i in range(len(word_to_guess)):
            if word[i] == word_to_guess[i]:
                ret += 'o'
            elif word[i] in word_to_guess:
                ret += 'p'
            else:
                ret += 'b'

        return ret

    def update_sets_on_word_status(self, status_word, user_word):
        assert(len(status_word) <= len(user_word))
        assert(len(status_word) <= len(self.arrLettersInPlace))
        assert(len(status_word) <= len(self.arrLettersNotInPlace))

        bBadWord = False

        for i in range(len(status_word)):
            if status_word[i] == 'o':
                self.setMandatoryLetters.add(user_word[i])
                self.setProhibitedLetters -= {user_word[i]}
                self.arrLettersInPlace[i] = user_word[i]
                self.arrLettersNotInPlace[i] -= {user_word[i]}
            elif status_word[i] == 'p':
                bBadWord = True
                self.setMandatoryLetters.add(user_word[i])
                self.setProhibitedLetters -= {user_word[i]}
                self.arrLettersNotInPlace[i].add(user_word[i])
            elif status_word[i] == 'b':
                bBadWord = True
                self.setProhibitedLetters.add(user_word[i])
                self.arrLettersNotInPlace[i].add(user_word[i])
            elif status_word[i] == '.':
                bBadWord = True
            else:
                assert(False)

        if bBadWord:
            self.setBadWords.add(user_word)

        self.setProhibitedLetters -= self.setMandatoryLetters

    def reset_play(self):
        self.setMandatoryLetters = set()
        self.setProhibitedLetters = set()
        self.arrLettersInPlace = ['', '', '', '', '', '']
        self.arrLettersNotInPlace = [set(), set(), set(), set(), set(), set()]
        self.setBadWords = set()

    def find_correct_words(self, num_of_letters: int, first_turn: bool):
        setSlowa = self.lenSets[f'len{num_of_letters}'] - self.setBadWords

        for letter in self.setProhibitedLetters:
            setSlowa = setSlowa - self.letterSets[letter]

        for letter in self.setMandatoryLetters:
            setSlowa = setSlowa & self.letterSets[letter]

        arrPasujaceSlowaAttr = []

        for slowo in setSlowa:
            bGood = True

            for i in range(len(slowo)):
                if (self.arrLettersInPlace[i] is not None) and (self.arrLettersInPlace[i] != ''):
                    if slowo[i] != self.arrLettersInPlace[i]:
                        bGood = False
                        break

                if slowo[i] in self.arrLettersNotInPlace[i]:
                    bGood = False
                    break

            if bGood:
                slowo_attr = self.dictWordsWithAttrs[slowo]
                arrPasujaceSlowaAttr.append(slowo_attr)

        if not self.tylko_podstawowe_slowa and first_turn:
            arrPasujaceSlowaAttr.sort(key=lambda x: x.letter_points, reverse=True)
        else:
            arrPasujaceSlowaAttr.sort()

        return arrPasujaceSlowaAttr

    def play(self):
        setSpecialCommand = ['-list']

        self.reset_play()

        status_word = 'start'

        word_to_guess = tools.input_colorama(
            f'{Style.RESET_ALL}Podaj słowo do zgadnięcia (lub pusty tekst): ', 
            pattern = f"^([a-ząćęłńóśżź]{ '{' }5,6{ '}' })?$", 
            errorMessage = f"Słowo musi się składać z dokładnie 5 lub 6 małych polskich liter lub być puste i musi być w słowniku.",
            onlyAllowedWords = self.lenSets['len5'] | self.lenSets['len6'] | {''})

        num_of_letters = len(word_to_guess)

        if num_of_letters == 0:
            num_of_letters = int(tools.input_colorama(
                'Podaj liczbę liter (5 lub 6): ', pattern = '^[56]$'))

        turn_number = 1

        while ('b' in status_word) or ('p' in status_word) or ((status_word != '') and (status_word != 'ooooo')):
            arrPasujaceSlowaAttr = self.find_correct_words(num_of_letters, turn_number <= 1)

            znalezione_slowo_attr = arrPasujaceSlowaAttr[0] if len(
                arrPasujaceSlowaAttr) > 0 else None
            liczba_pasujacych_slow = len(arrPasujaceSlowaAttr)
            liczba_pasujacych_slow_bazowych = 0
            for s in arrPasujaceSlowaAttr:
                if s.is_base_word: liczba_pasujacych_slow_bazowych += 1

            while True:
                slowo_wynik = tools.input_colorama(
                    f'{Style.RESET_ALL}Mam {liczba_pasujacych_slow} ({liczba_pasujacych_slow_bazowych}) słow, podaj własne: {Fore.GREEN}{Style.BRIGHT}', 
                    pattern = f"^([a-ząćęłńóśżź]{ '{' }{num_of_letters}{ '}' })?$", 
                    extraWords = setSpecialCommand,
                    errorMessage = f"Słowo musi się składać z dokładnie {num_of_letters} małych polskich liter lub być puste.")

                if CSlowaSolver.serve_special_words(slowo_wynik, arrPasujaceSlowaAttr):
                    continue

                break

            if slowo_wynik == '':
                slowo_wynik = znalezione_slowo_attr.word
                print(
                    f'{Style.RESET_ALL}spośród {liczba_pasujacych_slow} ({liczba_pasujacych_slow_bazowych}) słów proponuje: {Fore.GREEN}{Style.BRIGHT}{slowo_wynik}{Style.RESET_ALL} {znalezione_slowo_attr.cword_info()}')

            while True:
                if (word_to_guess != ''):
                    status_word = CSlowaSolver.calc_status_for_word(
                        word_to_guess, slowo_wynik)
                    print(
                        f'{Style.RESET_ALL}Wyliczony status tego słowa: {Fore.YELLOW}{Style.BRIGHT}{status_word}')
                else:
                    status_word = tools.input_colorama(
                        f'{Style.RESET_ALL}Podaj status tego słowa: {Fore.YELLOW}{Style.BRIGHT}', 
                        pattern = f"^[bop]{ '{' }{num_of_letters}{ '}' }$", 
                        extraWords = setSpecialCommand,
                        errorMessage=f"Dokładnie { num_of_letters } liter 'b' (bad letter), 'p' (bad position), 'o' (letter ok)")

                if CSlowaSolver.serve_special_words(status_word, arrPasujaceSlowaAttr):
                    continue

                break

            self.update_sets_on_word_status(status_word, slowo_wynik)
            turn_number += 1

    def find_hardest_word(self):

        min_findings = 0

        num_of_calulated_words = 0

        for num_of_letters in [5, 6]:
            setSlowa = self.lenSets[f'len{num_of_letters}']

            for slowo in setSlowa:
                if not slowo in self.setBaseWords:
                    continue

                num_of_calulated_words += 1

                self.reset_play()

                findings = 0

                turn_number = 1

                while True:
                    findings += 1
                    arrPasujaceSlowaAttr = self.find_correct_words(
                        num_of_letters, turn_number == 1)

                    if arrPasujaceSlowaAttr[0].word == slowo:
                        break

                    status_word = CSlowaSolver.calc_status_for_word(
                        slowo, arrPasujaceSlowaAttr[0].word)
                    self.update_sets_on_word_status(
                        status_word, arrPasujaceSlowaAttr[0].word)

                    turn_number += 1

                if findings > min_findings:
                    min_findings = findings

                    print(f"{slowo} - {min_findings} - {num_of_calulated_words}")

    def find_n_universal_words(self, numOfWords: int, wordLen: int, bOnlyBase: bool):
        print (f"finding {numOfWords} best words from {'only base' if bOnlyBase else 'all'} words, {wordLen} length each")
        self.reset_play()

        setSlowa = self.lenSets[f'len{wordLen}'] - self.setBadWords

        setUsedLetters = set()

        for i in range(numOfWords):
            bestWord = ''
            bestDifferentLetters = 0
            bestWordLetterPoints = 0

            for word in setSlowa:
                word_attr = self.dictWordsWithAttrs[word]

                if bOnlyBase and not word_attr.is_base_word:
                    continue

                # for every word I will calculate
                # how many different letters of the word was not used before
                setUnusedLetters = set()

                for letter in word_attr.word:
                    if not letter in setUsedLetters:
                        setUnusedLetters |= {letter}
    
                numOfNewDifferentLetters = len(setUnusedLetters)

                if (numOfNewDifferentLetters > bestDifferentLetters) or ((numOfNewDifferentLetters == bestDifferentLetters) and (word_attr.letter_points > bestWordLetterPoints)):
                    bestWord = word_attr.word
                    bestDifferentLetters = numOfNewDifferentLetters
                    bestWordLetterPoints = word_attr.letter_points

            for letter in bestWord:
                setUsedLetters |= {letter}

            print(bestWord)





