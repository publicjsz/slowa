#! /bin/python3

import argparse
import colorama

from word_with_attrs import CWordWithAttrs
from slowa_solver import CSlowaSolver


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('tylko_podstawowe_slowa', nargs="?", help="True/False w zalezności czy tylko pasujące słowa mają być", default=False, type=bool)
    args = arg_parser.parse_args()

    colorama.init()
    # os.system('cls')

    ss = CSlowaSolver(tylko_podstawowe_slowa=args.tylko_podstawowe_slowa)

    # start of playing

    while True:
        ss.play()
