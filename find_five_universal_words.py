#! /bin/python3

import colorama

from word_with_attrs import CWordWithAttrs
from slowa_solver import CSlowaSolver


if __name__ == '__main__':
    colorama.init()

    ss = CSlowaSolver()

    # start of playing

    ss.find_n_universal_words(numOfWords = 5, wordLen = 5, bOnlyBase = False)
    ss.find_n_universal_words(numOfWords = 5, wordLen = 5, bOnlyBase = True)
    ss.find_n_universal_words(numOfWords = 5, wordLen = 6, bOnlyBase = False)
    ss.find_n_universal_words(numOfWords = 5, wordLen = 6, bOnlyBase = True)
